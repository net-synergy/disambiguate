{
  description = "Author disambiguation";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pyVersion = "3.10";
        pkgs = nixpkgs.legacyPackages.${system};
        disambiguateEnv = pkgs.poetry2nix.mkPoetryEnv {
          projectDir = ./.;
          editablePackageSources = { disambiguate = ./disambiguate; };
          preferWheels = true;
          extraPackages = (ps: with ps; [ ipdb ]);
          groups = [ ];
          overrides = pkgs.poetry2nix.defaultPoetryOverrides.extend
            (self: super: {
              pubnet = super.pubnet.overridePythonAttrs (old: {
                buildInputs = (old.buildInputs or [ ]) ++ [ super.poetry-core ];
              });
            });
        };
        disambiguate = pkgs.poetry2nix.mkPoetryPackages { projectDir = ./.; };
      in {
        packages.disambiguate = disambiguate;
        packages.default = self.packages.${system}.disambiguate;
        devShells.default = pkgs.mkShell {
          packages = [ disambiguateEnv ]
            ++ (with pkgs; [ poetry astyle bear gdb gnumake ]);
          shellHook = ''
            # For use with bear to generate compile commands file.
            C_INCLUDE_PATH=${disambiguateEnv}/lib/python${pyVersion}/site-packages/numpy/core/include
            export C_INCLUDE_PATH=${disambiguateEnv}/include/python${pyVersion}:$C_INCLUDE_PATH
          '';
        };
      });
}
