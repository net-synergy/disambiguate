.PHONY: all
all: dist

.PHONY: dist
dist: clean-dist
	poetry build

.PHONY: %.py
%.py: dev-build
	ipython $*.py

.PHONY: dev-build
dev-build:
	python setup.py build_ext --inplace

.PHONY: compile_commands
compile_commands: clean-dist
	bear -- $(MAKE) all

.PHONY: clean-dist
clean-dist: clean
	-rm -rf build
	-rm -rf dist

.PHONY: clean
clean:
	-rm disambiguate/*.so
