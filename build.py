"""Script to build C extensions."""

import numpy as np
from setuptools import Extension
from setuptools.command.build_ext import build_ext
from setuptools.command.develop import develop
from setuptools.errors import CCompilerError, ExecError, PlatformError

_DEBUG = False
extra_compile_args = ["-O0", "-g3"] if _DEBUG else ["-O3", "-fopenmp"]

extensions = [
    Extension(
        "disambiguate._similarity",
        sources=["disambiguate/_similarity.c", "disambiguate/src/common.c"],
        include_dirs=[np.get_include(), "disambiguate/include"],
        extra_compile_args=extra_compile_args,
        extra_link_args=["-fopenmp"],
    ),
    Extension(
        "disambiguate._types",
        sources=["disambiguate/_types.c", "disambiguate/src/common.c"],
        include_dirs=[np.get_include(), "disambiguate/include"],
        extra_compile_args=extra_compile_args,
        extra_link_args=["-fopenmp"],
    ),
    Extension(
        "disambiguate._validate",
        sources=["disambiguate/_validate.c"],
        include_dirs=[np.get_include()],
        extra_compile_args=extra_compile_args,
        extra_link_args=["-fopenmp"],
    ),
]


class ExtBuilder(build_ext):
    def run(self):
        try:
            build_ext.run(self)
        except FileNotFoundError:
            print("Failed to build C extension.")

    def build_extension(self, ext):
        try:
            build_ext.build_extension(self, ext)
        except (
            CCompilerError,
            ExecError,
            PlatformError,
            ValueError,
        ):
            print("Failed to build C extension.")


class CustomDevelop(develop):
    def run(self):
        self.run_command("build_clib")
        super().run()


def build(setup_kwargs):
    setup_kwargs.update({
        "ext_modules": extensions,
        "cmdclass": {"develop": CustomDevelop},
    })
