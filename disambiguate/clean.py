__all__ = ["email", "orcid", "check_sum", "first_initial"]
import re

import numpy as np
from pubnet import PubNet

EMAIL_RE = re.compile(
    r"(?P<Address>[a-zA-Z0-9_\.+-]+\s*@\s*(?:\.?[a-zA-Z0-9-]+)+)"
)
ORCID_RE = re.compile(r"(?P<Identifier>\d{4}-\d{4}-\d{4}-\d{3}[0-9x]$)")
FIRST_INITIAL_RE = re.compile(r"^(?P<Initial>[a-z])")


def email(net: PubNet) -> None:
    net.mutate_node_re(
        "Email", EMAIL_RE, "Affiliation", "Affiliation", discard_used=True
    )


def orcid(net: PubNet) -> None:
    net.mutate_node_re("Orcid", ORCID_RE, "Orcid", "Identifier")


def first_initial(net: PubNet) -> None:
    def rule(node):
        return np.fromiter(
            (
                (i, name[0])
                for i, name in zip(node.index, node.feature_vector("ForeName"))
                if name
            ),
            dtype=np.dtype((object, 2)),
        )

    net.mutate_node("FirstInitial", "ForeName", rule)


def check_sum(orcid: str, tally: int = 0) -> bool:
    if len(orcid) == 1:
        return ((12 - (tally % 11)) % 11) == (
            10 if orcid[0] == "x" else int(orcid[0])
        )

    return check_sum(
        orcid[1:], tally if orcid[0] == "-" else (tally + int(orcid[0])) * 2
    )
