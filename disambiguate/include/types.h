#ifndef TYPES_H
#define TYPES_H

#include <Python.h>
#include <numpy/arrayobject.h>

/* EdgeList represents edges in a sparse format using flat arrays.

The rows field has size n_rows + 1 and represents the offset of the flat
arrays to get the first value of the row. The first element will therefore
always be 0 and the rows[i + 1] - rows[i] will be the number of non-zero
elements in the given row. As such, the value rows[n_rows] is the total
number of non-zero values.

Columns represents the column index of the values in the given row.
Weights are the values (can be a NULL pointer, in which case all non-zero
weights are 1).

The columns and weights fields are both 1-d Arrays of the same length (number of
non-zero values in the list). */
typedef struct {
  PyObject_HEAD
  size_t *rows;
  size_t *cols;
  size_t *authors; // Special index for Author publication edges.
  double *weights;
  size_t n_rows;
  size_t n_cols;
  size_t n_unique_rows;
} EdgeList;

#define EDGE_NROW(e) (e)->n_rows
#define EDGE_NNZ(e) (e)->rows[(e)->n_rows]
#define EDGE_IROW(e, row_i) (e)->rows[(row_i)]
#define EDGE_COL(e, idx) (e)->cols[(idx)]
#define EDGE_WEIGHT(e, idx) ((e)->weights ? (e)->weights[(idx)] : 1.0)
#define EDGE_ROW_LENGTH(e, row_i) \
    (EDGE_IROW((e), (row_i) + 1) - EDGE_IROW((e), (row_i)))

typedef struct {
  EdgeList **edges;
  double *weights;
  size_t n;
  size_t author_pos;
} EdgeSet;

#define ES_IS_AUTHOR(e, idx) ((e)->author_pos == idx)
EdgeSet *collect_edges(PyObject *edge_dict, PyObject *weight_dict);
void edge_set_dealloc(EdgeSet *);

typedef struct {
  PyArrayObject *id;
  npy_intp len;
  size_t n_labels;
  size_t *tally;
} Label;

#define LABEL(label, idx) *(npy_int *)PyArray_GETPTR1((label)->id, idx)

void EL_tfidf(EdgeList *);
void EL_sort_rows(EdgeList *);
void label_dealloc(Label *labels);

#endif
