"""Methods for comparing authors in a publication network."""

__all__ = ["merge_within", "merge_between", "to_edgelist", "score_features"]

import numpy as np
import pandas as pd
from pubnet import PubNet

import disambiguate._similarity as sim
from disambiguate._types import EdgeList


def to_edgelist(
    net: PubNet, nodes: tuple[str, ...], tfidf: bool = True
) -> dict[str, EdgeList]:
    """Convert a publication network to a dictionary of EdgeLists.

    The root of the publication net will be used as the base for all resulting
    EdgeLists. So publication level feature will be returned for a network with
    publication root and author level features for a network with an author
    root.

    Arguments:
    ---------
    net : PubNet
      The network containing the edges to convert to EdgeLists. As mentioned
      above be aware of the network's root.

    nodes : tuple of strings
      A list of nodes that have a corresponding root--node edge set in the
      network to be converted to EdgeLists. The nodes names will be reused for
      the keys in the resulting dictionary.

    tfidf : bool
      Whether to calculate term-frequency inverse document frequency weights
      for the EdgeLists. If `True`, values that are more common across the edge
      set are given smaller weights. If `False`, all weights are left to 1.

    Returns:
    -------
    edge_dict : dict of EdgeLists
      A dictionary containing an EdgeList for each of the nodes in the `nodes`
      argument.
    """
    res = {}
    for n in nodes:
        edge = net.get_edge(net.root, n)
        indices = np.argsort(edge[net.root], 0)
        res[n] = EdgeList(
            np.take_along_axis(edge[net.root], indices, 0),
            np.take_along_axis(edge[n], indices, 0),
            tfidf=tfidf,
        )

    return res


def merge_within(
    labels: np.ndarray,
    pub_edges: dict[str, EdgeList],
    author_edges: dict[str, EdgeList],
    pub_weights: dict[str, float],
    author_weights: dict[str, float],
    threshold: float = 0.7,
) -> None:
    """Refine current labels by clustering authors within the label.

    This method loops over all labels and if there are multiple authors in the
    current label, it will perform within label merging based on similarity
    (dot product) of the provided features (pub_edges, author_edges).
    Similarity is calculated between all pairs of authors within a given label
    for each feature. The feature similarities are then combined using a
    weighted mean. If the weighted mean is greater than the provided threshold
    the authors will be grouped together. Each within label group will become a
    new label after merging. So this method creates new labels but does not
    remove/merge preexisting labels.

    Arguments:
    ---------
    labels : np.ndarray
      A list of initial labels, this must have length equal to the number of
      unique author--publication pairs. The order of the label should be the
      order of the author ids such that `label[id]` is the label for the author
      identified by `id`. This will be updated in place. If the old labels are
      needed, make sure to make a copy (with `labels.copy()`) before running.

    pub_edges : dict of EdgeLists
      A dictionary of EdgeLists based on publications (i.e. the rows should be
      publication ids). The dictionary keys should be the other nodes name.
      Author must be one of the EdgeLists. These provide the publication level
      features (references, keywords, mesh terms, etc). If it is not desired to
      use co-authorship as part of the similarity score, the author weight can
      be set to 0.

    author_edges : dict of EdgeLists
      Same as `pub_edges` except based on authors instead of publications. This
      provides author level features (author names, affiliations, etc). No keys
      are required.

    pub_weights : dict of floats
    author_weights : dict of doubles
      The weights for each feature. These should have the same keys as the
      corresponding edge dictionaries and the sum of the weights must be 1. Any
      feature whose weight is 0 will be ignored by the calculation so there is
      no need to remove EdgeLists that won't be used from the above
      dictionaries for efficiency purposes.

    threshold : float
      The minimum similarity score required to match two authors.
    """
    sim.merge_authors(
        "within",
        labels,
        pub_edges,
        author_edges,
        pub_weights,
        author_weights,
        threshold,
    )


def merge_between(
    labels: np.ndarray,
    pub_edges: dict[str, EdgeList],
    author_edges: dict[str, EdgeList],
    pub_weights: dict[str, float],
    author_weights: dict[str, float],
    threshold: float = 0.7,
) -> None:
    sim.merge_authors(
        "between",
        labels,
        pub_edges,
        author_edges,
        pub_weights,
        author_weights,
        threshold,
    )


def score_features(
    labels: np.ndarray,
    ground_truth: EdgeList,
    pub_edges: dict[str, EdgeList],
    min_occurances: int = 2,
) -> pd.DataFrame:
    return pd.DataFrame(
        sim.score_features(labels, ground_truth, pub_edges, min_occurances)
    )
