#define PY_SSIZE_T_CLEAN
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <omp.h>
#include <time.h>

#include "types.h"

#define MAX(a, b) (a) > (b) ? (a) : (b)

static int check_weights(EdgeSet *pub_edges, EdgeSet *author_edges)
{
  double eps = 1E-5;
  double acc = 0;

  for (size_t i = 0; i < pub_edges->n; i++) {
    acc += pub_edges->weights[i];
  }

  for (size_t i = 0; i < author_edges->n; i++) {
    acc += author_edges->weights[i];
  }

  if ((acc < (1 - eps)) || (acc > (1 + eps))) {
    return -1;
  }

  return 0;
}

static void label_author_edges(EdgeList *edges, Label *labels)
{
  if (!edges->authors) {
    edges->authors = edges->cols;
    edges->cols = PyMem_Malloc(sizeof(*edges->cols) * EDGE_NNZ(edges));
  }

  for (size_t i = 0; i < EDGE_NNZ(edges); i++) {
    edges->cols[i] = (size_t)LABEL(labels, i);
  }

  edges->n_cols = 0;
  for (size_t i = 0; i < EDGE_NNZ(edges); i++) {
    edges->n_cols = MAX(edges->n_cols, edges->cols[i]);
  }
  edges->n_cols++;

  EL_sort_rows(edges);
  if (edges->weights) {
    EL_tfidf(edges);
  }
}

static void count_labels(Label *labels, EdgeList *authors)
{
  /* Assumes subnet contains all labels, this may not be true, but total number
   of labels should be small enough compared to other values to not be worth
   condensing (i.e. so we can continue to use raw labels as indices). */
  labels->n_labels = 0;
  for (npy_intp i = 0; i < labels->len; i++) {
    labels->n_labels = MAX(labels->n_labels, (size_t)LABEL(labels, i));
  }
  labels->n_labels++;

  labels->tally = (size_t *)PyMem_Calloc(labels->n_labels,
                                         sizeof(*(labels->tally)));
  for (size_t i = 0; i < EDGE_NNZ(authors); i++) {
    labels->tally[EDGE_COL(authors, i)]++;
  }
}


static inline void swap_label(size_t *labels, size_t i, size_t j)
{
  npy_int64 tmp = labels[i];
  labels[i] = labels[j];
  labels[j] = tmp;
}

static inline size_t random_draw(size_t i, size_t n)
{
  return (random() % n);
}

static void shuffle(size_t *labels, size_t n_labels)
{
  // True randomness not important.
  srandom(0);
  for (size_t i = 0; i < n_labels; i++) {
    labels[i] = i;
  }

  for (size_t i = 0; i < n_labels; i++) {
    swap_label(labels, i, random_draw(i, n_labels));
  }
}

static inline void collect_tag_members(const EdgeList *pub_author,
                                       const size_t tag,
                                       const Label *labels,
                                       size_t *publication_ids,
                                       size_t *author_ids)
{
  size_t count = 0;
  for (size_t pub_i = 0; pub_i < EDGE_NROW(pub_author); pub_i++) {
    for (size_t author_i = EDGE_IROW(pub_author, pub_i);
         author_i < EDGE_IROW(pub_author, pub_i + 1); author_i++) {
      if (LABEL(labels, author_i) == (npy_int64)tag) {
        publication_ids[count] = pub_i;
        author_ids[count] = pub_author->authors[author_i];
        count++;
      }
    }
  }
}

/* NOTE: It's possible a vector can have multiple values at the same index.
   This can happen if a paper has multiple authors with the same label or in
   the case a feature isn't unique (multiple mesh terms can have the same
   qualifier). In this case, the element is not given extra weight. As such sum
   of the squares will be less than the dot product (since the dot product does
   take into account duplicates.) and dot products may end up being greater
   than 1.0. For now this is ignored as it seems rare but may be addressed
   later.*/
static inline void calc_ssq(EdgeSet *es, double **ssq, const size_t tag,
                            const size_t *ids, const size_t len)
{
  EdgeList *el;
  for (size_t es_i = 0; es_i < es->n; es_i++) {
    if (es->weights[es_i] == 0) {
      continue;
    }

    el = es->edges[es_i];
    for (size_t i = 0; (ids[i] < EDGE_NROW(el)) && (i < len); i++) {
      for (size_t col_i = EDGE_IROW(el, ids[i]);
           col_i < EDGE_IROW(el, ids[i] + 1); col_i++) {
        // Skip the author of interest when taking co-author dot product.
        if (!(ES_IS_AUTHOR(es, es_i) && (EDGE_COL(el, col_i) == tag))) {
          ssq[es_i][i] += EDGE_WEIGHT(el, col_i) * EDGE_WEIGHT(el, col_i);
        }
      }
    }

    /* Assuming non-negative weights, the only way for ssq to be zero is if
    either the vector has length 0 or there are no non-zero weights. In either
    case the dot-product between that vector and any other vector must be 0.
    Since the dot-product is normalized by the sqrt(ssq) for both vectors, if
    one vector's ssq is 0 the dot-product will be zero and therefore ssq can be
    changed to anything other than zero to prevent divide-by-zero errors
    without impacting the results. */
    for (size_t i = 0; i < len; i++) {
      ssq[es_i][i] = ssq[es_i][i] ? sqrt(ssq[es_i][i]) : 1.0;
    }
  };
}

static inline void merge_labels(const npy_int64 comm1, const npy_int64 comm2,
                                npy_int64 *labels, const size_t n_labels)
{
  for (size_t i = 0; i < n_labels; i++) {
    if (labels[2 * i] == comm2) {
      labels[2 * i] = comm1;
    }
  }
}

static inline void repack_labels(npy_int64 *labels, const size_t n_labels)
{
  int is_num_used[n_labels];
  size_t shifts[n_labels];

  for (size_t i = 0; i < n_labels; i++) {
    is_num_used[i] = 0;
    shifts[i] = 0;
  }

  for (size_t i = 0; i < n_labels; i++) {
    is_num_used[labels[2 * i]] = 1;
  }

  for (size_t i = 0; i < (n_labels - 1); i++) {
    if (!is_num_used[i]) {
      shifts[i + 1]++;
    }
  }

  for (size_t i = 0; i < (n_labels - 1); i++) {
    shifts[i + 1] += shifts[i];
  }

  for (size_t i = 0; i < n_labels; i++) {
    labels[2 * i] -= shifts[labels[2 * i]];
  }
}

static void within_cluster(const size_t tag, EdgeSet *pub_es,
                           EdgeSet *author_es, const double threshold,
                           npy_int64 *new_labels, const Label *old_labels)
{
  size_t n_members = old_labels->tally[tag];
  size_t publication_ids[n_members];
  size_t author_ids[n_members];
  double **pub_ssq;

  collect_tag_members(pub_es->edges[pub_es->author_pos], tag, old_labels,
                      publication_ids, author_ids);
  for (npy_int64 i = 0; i < (npy_int64)n_members; i++) {
    new_labels[2 * i] = i;
    new_labels[(2 * i) + 1] = author_ids[i];
  }

  if (n_members <= 1) {
    return;
  }

  /* NOTE: For within type merging, author components will have length 1, so
  don't need to calculate sqrt of sum of squares for them. */
  pub_ssq = malloc(pub_es->n * sizeof(*pub_ssq));
  for (size_t i = 0; i < pub_es->n; i++) {
    pub_ssq[i] = calloc(n_members, sizeof(**pub_ssq));
  }
  calc_ssq(pub_es, pub_ssq, tag, publication_ids, n_members);

  EdgeList *el;
  size_t pub_i, pub_j, author_i, author_j;
  for (size_t i = 0; i < (n_members - 1); i++) {
    pub_i = publication_ids[i];
    author_i = author_ids[i];
    for (size_t j = (i + 1); j < n_members; j++) {
      double similarity = 0;
      pub_j = publication_ids[j];
      author_j = author_ids[j];

      for (size_t type_i = 0; type_i < pub_es->n; type_i++) {
        double component_similarity = 0;
        el = pub_es->edges[type_i];

        if ((pub_es->weights[type_i] == 0) ||
            (EDGE_NROW(el) < pub_j) ||
            (EDGE_ROW_LENGTH(el, pub_i) == 0) ||
            (EDGE_ROW_LENGTH(el, pub_j) == 0)) {
          continue;
        }

        for (size_t col_i = EDGE_IROW(el, pub_i);
             col_i < EDGE_IROW(el, pub_i + 1); col_i++) {
          size_t col_j;
          for (col_j = EDGE_IROW(el, pub_j);
               (col_j < EDGE_IROW(el, pub_j + 1)) &&
               (EDGE_COL(el, col_j) < EDGE_COL(el, col_i)); col_j++);

          if ((EDGE_COL(el, col_j) == EDGE_COL(el, col_i)) &&
              !(ES_IS_AUTHOR(pub_es, type_i) &&
                (EDGE_COL(el, col_j) == tag))) {
            component_similarity +=
              EDGE_WEIGHT(el, col_j) * EDGE_WEIGHT(el, col_i);
          }
        }

        similarity += (pub_es->weights[type_i] * component_similarity /
                       (pub_ssq[type_i][i] * pub_ssq[type_i][j]));
      }

      for (size_t type_i = 0; type_i < author_es->n; type_i++) {
        el = author_es->edges[type_i];
        size_t col_i = EDGE_IROW(el, author_i);
        size_t col_j = EDGE_IROW(el, author_j);

        if ((author_es->weights[type_i] == 0) ||
            (EDGE_NROW(el) < author_j) ||
            (EDGE_ROW_LENGTH(el, author_i) == 0) ||
            (EDGE_ROW_LENGTH(el, author_j) == 0)) {
          continue;
        }

        if (EDGE_COL(el, col_i) == EDGE_COL(el, col_j)) {
          similarity += author_es->weights[type_i];
        }
      }

      if (similarity > threshold) {
        merge_labels(i, j, new_labels, n_members);
      }
    }
  }

  repack_labels(new_labels, n_members);

  for (size_t i = 0; i < pub_es->n; i++) {
    free(pub_ssq[i]);
  }
  free(pub_ssq);
}

static void sim_within(Label *labels, EdgeSet *pub_es,
                       EdgeSet *author_es,
                       const double threshold)
{
  npy_int64 **label_labels;
  size_t randomized_labels[labels->n_labels];

  label_labels = (npy_int64 **)PyMem_RawMalloc(labels->n_labels *
                 sizeof(*label_labels));
  for (npy_int64 i = 0; i < (npy_int64)labels->n_labels; i++) {
    /* 2 * label count to track both position in original label array and new
    label. */
    label_labels[i] = (npy_int64 *)PyMem_RawMalloc(2 * labels->tally[i] *
                      sizeof(**label_labels));
  }

  shuffle(randomized_labels, labels->n_labels);

  struct timespec start, finish;
  double elapsed;

  clock_gettime(CLOCK_MONOTONIC, &start);
  #pragma omp parallel for
  for (size_t i = 0; i < labels->n_labels; i++) {
    /* Since original labels are ordered based on the first time a given name
    is seen, more common names are more likely to be seen early on and
    therefore get the lower label numbers. OMP's default scheduler gives the
    first core the first n iterations, the second core the next n and so on.
    This leads to the first core being bound to handling the largest labels. By
    randomizing the order of labels, the larger labels are distributed more
    evenly across cores. */
    size_t tag = randomized_labels[i];
    within_cluster(tag, pub_es, author_es, threshold,
                   label_labels[tag], labels);
  }
  clock_gettime(CLOCK_MONOTONIC, &finish);

  elapsed = finish.tv_sec - start.tv_sec;
  elapsed += (finish.tv_nsec - start.tv_nsec) * 1.0E-9;
  printf("time: %f\n", elapsed);

  npy_int64 prev_max = 0, max_label = 0, pos = 0;
  for (npy_int64 i = 0; i < (npy_int64)labels->n_labels; i++) {
    prev_max = max_label;
    for (npy_int64 j = 0; j < (npy_int64)labels->tally[i]; j++) {
      pos = label_labels[i][(2 * j) + 1];
      LABEL(labels, pos) = label_labels[i][2 * j] + prev_max;
      max_label = MAX(max_label, LABEL(labels, pos));
    }
    max_label++;

    PyMem_RawFree(label_labels[i]);
  }
  PyMem_RawFree(label_labels);
}

static PyObject *sim_merge(PyObject *dummy, PyObject *args)
{
  char *type;
  PyObject *pub_edges_in, *author_edges_in, *pub_weights_in, *author_weights_in;
  PyObject *labels_obj, *labels_in;
  double threshold;

  if (!PyArg_ParseTuple(args, "sOO!O!O!O!d",
                        &type,
                        &labels_obj,
                        &PyDict_Type, &pub_edges_in,
                        &PyDict_Type, &author_edges_in,
                        &PyDict_Type, &pub_weights_in,
                        &PyDict_Type, &author_weights_in,
                        &threshold)) {
    return NULL;
  }

  labels_in = PyArray_FROM_OTF(labels_obj, NPY_INT64,
                               NPY_ARRAY_INOUT_ARRAY);

  if (!PyDict_Contains(pub_edges_in, PyUnicode_FromString("Author"))) {
    PyErr_SetString(PyExc_ValueError,
                    "pub_edges must contain Publication--Author edges.");
    return NULL;
  }

  if (!((threshold >= 0) && (threshold <= 1))) {
    PyErr_SetString(PyExc_ValueError, "Threshold must be in range [0, 1]");
  }

  EdgeSet *pub_es = collect_edges(pub_edges_in, pub_weights_in);
  EdgeSet *author_es = collect_edges(author_edges_in, author_weights_in);

  Label labels = {
    .id = (PyArrayObject *)labels_in,
    .len = PyArray_DIM((PyArrayObject *)labels_in, 0),
  };

  if (check_weights(pub_es, author_es) < 0) {
    PyErr_SetString(PyExc_ValueError,
                    "Weights must sum to 1.");
    return NULL;
  };

  label_author_edges(pub_es->edges[pub_es->author_pos], &labels);
  count_labels(&labels, pub_es->edges[pub_es->author_pos]);

  if (strcmp(type, "within") == 0) {
    sim_within(&labels, pub_es, author_es, threshold);
  } else if (strcmp(type, "between") == 0) {
    PyErr_SetString(PyExc_NotImplementedError, "Between not implemented yet.");
    return NULL;
  } else {
    PyErr_SetString(PyExc_ValueError, "Type must be \"within\" or \"between\".");
    return NULL;
  }

  edge_set_dealloc(pub_es);
  edge_set_dealloc(author_es);
  label_dealloc(&labels);

  return labels_in;
}

static size_t **gt_collect_ids(const EdgeList *gt, const Label *labels,
                               const EdgeList *pub_author,
                               size_t *offsets, size_t *pair_offsets,
                               const size_t min_occurances)
{
  size_t **ret = (size_t **)PyMem_Malloc(2 * sizeof(**ret));

  size_t n_authors = 0;
  for (size_t i = 0; i < EDGE_NNZ(pub_author); i++) {
    n_authors = MAX(n_authors, pub_author->authors[i]);
  }
  n_authors++;

  size_t *counts = (size_t *)PyMem_Calloc(gt->n_cols, sizeof(*counts));
  for (size_t i = 0; i < EDGE_NNZ(gt); i++) {
    counts[EDGE_COL(gt, i)]++;
  }

  for (size_t i = 0; i <= labels->n_labels; i++) {
    offsets[i] = 0;
    pair_offsets[i] = 0;
  }

  for (size_t i = 0; (i < EDGE_NROW(gt)) && (i < n_authors); i++) {
    for (size_t j = EDGE_IROW(gt, i); j < EDGE_IROW(gt, i + 1); j++) {
      offsets[LABEL(labels, i) + 1] += counts[EDGE_COL(gt, j)] >= min_occurances;
    }
  }

  for (size_t i = 1; i <= labels->n_labels; i++) {
    pair_offsets[i] = pair_offsets[i - 1] +
                      (offsets[i] * (offsets[i] - 1) / 2);
    offsets[i] += offsets[i - 1];
  }

  size_t *id_offsets = (size_t *)PyMem_Malloc(labels->n_labels *
                       sizeof(*id_offsets));
  for (size_t i = 0; i < labels->n_labels; i++) {
    id_offsets[i] = offsets[i];
  }

  size_t *pub_indices = (size_t *)PyMem_Malloc(n_authors *
                        sizeof(*pub_indices));
  for (size_t i = 0; i < EDGE_NROW(pub_author); i++) {
    for (size_t j = EDGE_IROW(pub_author, i); j < EDGE_IROW(pub_author, i + 1);
         j++) {
      pub_indices[pub_author->authors[j]] = i;
    }
  }

  size_t *publication_ids = (size_t *)PyMem_Malloc(offsets[labels->n_labels] *
                            sizeof(*publication_ids));
  size_t *gt_ids = PyMem_Malloc(offsets[labels->n_labels] * sizeof(*gt_ids));
  for (size_t i = 0; i < EDGE_NROW(gt); i++) {
    for (size_t j = EDGE_IROW(gt, i); j < EDGE_IROW(gt, i + 1); j++) {
      if ((counts[EDGE_COL(gt, j)] >= min_occurances) && (i < n_authors)) {
        size_t idx = id_offsets[LABEL(labels, i)];
        publication_ids[idx] = pub_indices[i];
        gt_ids[idx] = EDGE_COL(gt, j);
        id_offsets[LABEL(labels, i)]++;
      }
    }
  }

  PyMem_Free(pub_indices);
  PyMem_Free(id_offsets);
  PyMem_Free(counts);

  ret[0] = publication_ids;
  ret[1] = gt_ids;
  return ret;
}

static void gt_within_similarity(EdgeSet *pub_es,
                                 const size_t *publication_ids,
                                 const size_t *gt_ids,
                                 const size_t tag,
                                 const size_t *gt_offsets,
                                 const size_t *gt_pair_offsets,
                                 npy_bool *mask, npy_float **scores)
{
  size_t n_members = gt_offsets[tag + 1] - gt_offsets[tag];
  double **pub_ssq = malloc(pub_es->n * sizeof(*pub_ssq));
  for (size_t i = 0; i < pub_es->n; i++) {
    pub_ssq[i] = calloc(n_members, sizeof(*pub_ssq[i]));
  }
  calc_ssq(pub_es, pub_ssq, tag, publication_ids, n_members);

  EdgeList *el;
  size_t pub_i, pub_j;
  size_t count = gt_pair_offsets[tag];
  for (size_t i = 0; i < (n_members - 1); i++) {
    pub_i = publication_ids[i];
    for (size_t j = (i + 1); j < n_members; j++) {
      pub_j = publication_ids[j];
      mask[count] = gt_ids[i] == gt_ids[j];

      for (size_t type_i = 0; type_i < pub_es->n; type_i++) {
        scores[type_i][count] = 0;
        el = pub_es->edges[type_i];

        if ((EDGE_NROW(el) < pub_j) ||
            (EDGE_NROW(el) < pub_i) ||
            (EDGE_ROW_LENGTH(el, pub_i) == 0) ||
            (EDGE_ROW_LENGTH(el, pub_j) == 0)) {
          continue;
        }

        for (size_t col_i = EDGE_IROW(el, pub_i);
             col_i < EDGE_IROW(el, pub_i + 1); col_i++) {
          size_t col_j;
          for (col_j = EDGE_IROW(el, pub_j);
               (col_j < EDGE_IROW(el, pub_j + 1)) &&
               (EDGE_COL(el, col_j) < EDGE_COL(el, col_i)); col_j++);

          if ((EDGE_COL(el, col_j) == EDGE_COL(el, col_i)) &&
              !(ES_IS_AUTHOR(pub_es, type_i) &&
                (EDGE_COL(el, col_j) == tag))) {
            scores[type_i][count] +=
              EDGE_WEIGHT(el, col_j) * EDGE_WEIGHT(el, col_i);
          }
        }

        scores[type_i][count] /= (pub_ssq[type_i][i] * pub_ssq[type_i][j]);
      }

      count++;
    }
  }

  for (size_t i = 0; i < pub_es->n; i++) {
    free(pub_ssq[i]);
  }
  free(pub_ssq);
}

static PyObject *sim_score_ground_truth(PyObject *dummy, PyObject *args)
{
  PyObject *pub_edges_in;
  PyObject *labels_obj, *labels_in, *gt_obj;
  EdgeList *gt;
  PyObject *res = PyDict_New();
  size_t min_occurances;

  if (!PyArg_ParseTuple(args, "OOO!k",
                        &labels_obj,
                        &gt_obj,
                        &PyDict_Type, &pub_edges_in,
                        &min_occurances)) {
    return NULL;
  }

  labels_in = PyArray_FROM_OTF(labels_obj, NPY_INT64,
                               NPY_ARRAY_INOUT_ARRAY);
  gt = (EdgeList *)gt_obj;

  if (!PyDict_Contains(pub_edges_in, PyUnicode_FromString("Author"))) {
    PyErr_SetString(PyExc_ValueError,
                    "pub_edges must contain Publication--Author edges.");
    return NULL;
  }

  EdgeSet *pub_es = collect_edges(pub_edges_in, NULL);
  Label labels = {
    .id = (PyArrayObject *)labels_in,
    .len = PyArray_DIM((PyArrayObject *)labels_in, 0),
  };

  label_author_edges(pub_es->edges[pub_es->author_pos], &labels);
  count_labels(&labels, pub_es->edges[pub_es->author_pos]);

  size_t *gt_offsets = (size_t *)PyMem_Malloc((labels.n_labels + 1) *
                       sizeof(*gt_offsets));
  size_t *gt_pair_offsets = (size_t *)PyMem_Malloc((labels.n_labels + 1) *
                            sizeof(*gt_pair_offsets));
  size_t **ids = gt_collect_ids(gt, &labels, pub_es->edges[pub_es->author_pos],
                                gt_offsets, gt_pair_offsets, min_occurances);
  size_t *publication_ids = ids[0];
  size_t *gt_ids = ids[1];
  PyMem_Free(ids);

  size_t gt_n_pairs = gt_pair_offsets[labels.n_labels];
  npy_intp dims[] = { gt_n_pairs };
  npy_float **scores = (npy_float **)PyMem_Malloc(pub_es->n * sizeof(**scores));
  npy_bool *mask = (npy_bool *)PyMem_Malloc(gt_n_pairs * sizeof(*mask));

  for (size_t i = 0; i < pub_es->n; i++) {
    scores[i] = (npy_float *)PyMem_Malloc(gt_n_pairs * sizeof(**scores));
  }

  #pragma omp parallel for
  for (size_t tag = 0; tag < labels.n_labels; tag++) {
    if ((gt_offsets[tag + 1] - gt_offsets[tag]) <= 1) {
      continue;
    }

    gt_within_similarity(pub_es, publication_ids + gt_offsets[tag],
                         gt_ids + gt_offsets[tag], tag, gt_offsets,
                         gt_pair_offsets, mask, scores);
  }
  PyMem_Free(gt_offsets);
  PyMem_Free(gt_pair_offsets);
  PyMem_Free(publication_ids);
  PyMem_Free(gt_ids);

  PyDict_SetItem(res, PyUnicode_FromString("Match"),
                 PyArray_SimpleNewFromData(1, dims, NPY_BOOL,
                     mask));

  PyObject *key, *value;
  Py_ssize_t pos = 0;
  while (PyDict_Next(pub_edges_in, &pos, &key, &value)) {
    PyDict_SetItem(res, key, PyArray_SimpleNewFromData(1, dims, NPY_FLOAT,
                   scores[pos - 1]));
  }

  return res;
}

static PyMethodDef SimilarityMethods[] = {
  {"merge_authors", sim_merge, METH_VARARGS, NULL},
  {"score_features", sim_score_ground_truth, METH_VARARGS, NULL},
  {NULL, NULL, 0, NULL}
};

static struct PyModuleDef similarity_module = {
  .m_base = PyModuleDef_HEAD_INIT,
  .m_name = "_similarity",
  .m_size = -1,
  .m_methods = SimilarityMethods
};

PyMODINIT_FUNC PyInit__similarity(void)
{
  import_array();

  return PyModule_Create(&similarity_module);
}
