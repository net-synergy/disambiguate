#define PY_SSIZE_T_CLEAN
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "types.h"

static int cmp_col_indices(const void *p1, const void *p2)
{
  return *(size_t *)p1 - *(size_t *)p2;
}

void EL_sort_rows(EdgeList *edges)
{
  #pragma omp parallel for
  for (size_t row = 0; row < EDGE_NROW(edges); row++) {
    size_t *row_ptr = &(edges->cols[EDGE_IROW(edges, row)]);
    size_t row_len = EDGE_IROW(edges, row + 1) - EDGE_IROW(edges, row);
    qsort(row_ptr, row_len, sizeof(size_t),
          cmp_col_indices);
  }
}

void EL_tfidf(EdgeList *edges)
{
  if (!edges->weights) {
    edges->weights = (double *)PyMem_Malloc(sizeof(*(edges->weights)) *
                                            EDGE_NNZ(edges));
  }

  size_t *tally = (size_t *)PyMem_Calloc(edges->n_cols, sizeof(*tally));
  for (size_t i = 0; i < EDGE_NNZ(edges); i++) {
    tally[EDGE_COL(edges, i)] = 1;
  }

  for (size_t i = 0; i < EDGE_NNZ(edges); i++) {
    edges->weights[i] = log(edges->n_unique_rows /
                            tally[EDGE_COL(edges, i)]);
  }

  PyMem_Free(tally);
}

void label_dealloc(Label *labels)
{
  if (labels->tally) {
    PyMem_Free(labels->tally);
  }
}

EdgeSet *collect_edges(PyObject *edge_dict, PyObject *weight_dict)
{
  EdgeSet *es = PyMem_Malloc(sizeof(EdgeSet));
  EdgeSet es_tmp;
  es_tmp.n = PyDict_Size(edge_dict);
  es_tmp.edges = PyMem_Malloc(sizeof(*(es_tmp.edges)) * es_tmp.n);
  es_tmp.weights = PyMem_Malloc(sizeof(*(es_tmp.weights)) * es_tmp.n);
  es_tmp.author_pos = -1;

  PyObject *key, *value;
  Py_ssize_t pos = 0;
  while (PyDict_Next(edge_dict, &pos, &key, &value)) {
    Py_INCREF(value);
    es_tmp.edges[pos - 1] = (EdgeList *)value;

    if (weight_dict) {
      es_tmp.weights[pos - 1] = PyFloat_AsDouble(PyDict_GetItem(weight_dict, key));
    } else {
      es_tmp.weights[pos - 1] = 1;
    }

    if (PyUnicode_CompareWithASCIIString(key, "Author") == 0) {
      es_tmp.author_pos = pos - 1;
    }
  }

  memcpy(es, &es_tmp, sizeof(es_tmp));
  return es;
}

void edge_set_dealloc(EdgeSet *es)
{
  for (size_t i = 0; i < es->n; i++) {
    Py_DECREF(es->edges[i]);
  }

  if (es->weights) {
    PyMem_Free(es->weights);
  }

  PyMem_Free(es->edges);
  PyMem_Free(es);
}
