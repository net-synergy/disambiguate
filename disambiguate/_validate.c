#define PY_SSIZE_T_CLEAN
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <numpy/arrayobject.h>
#include <omp.h>
#include <time.h>

static void confusion_matrix(PyArrayObject *actual, PyArrayObject *expected,
                             size_t *tp, size_t *fp, size_t *fn, size_t *tn)
{
  size_t res_tp = 0, res_fp = 0, res_fn = 0, res_tn = 0;
  npy_intp n = PyArray_DIM(actual, 0);
  int pair_actual, pair_expected;

  #pragma omp parallel for reduction(+:res_tp,res_fp,res_fn,res_tn)
  for (npy_intp i = 0; i < (n - 1); i++) {
    for (npy_intp j = (i + 1); j < n; j++) {
      pair_actual = *(npy_int *)PyArray_GETPTR1(actual, i) ==
                    *(npy_int *)PyArray_GETPTR1(actual, j);
      pair_expected = *(npy_int *)PyArray_GETPTR1(expected, i) ==
                      *(npy_int *)PyArray_GETPTR1(expected, j);

      if (pair_actual && pair_expected) {
        res_tp++;
      } else if (pair_actual) {
        res_fp++;
      } else if (pair_expected) {
        res_fn++;
      } else {
        res_tn++;
      }
    }
  }

  *tp = res_tp;
  *fp = res_fp;
  *fn = res_fn;
  *tn = res_tn;
}

static PyObject *validation_template(PyObject *args,
                                     double (validation_func)(size_t, size_t,
                                         size_t, size_t))
{
  PyArrayObject *actual, *expected;
  double score;

  if (!PyArg_ParseTuple(args, "O!O!",
                        &PyArray_Type, &actual,
                        &PyArray_Type, &expected)) {
    return NULL;
  }

  npy_intp n_actual = PyArray_DIM(actual, 0);
  npy_intp n_expected = PyArray_DIM(expected, 0);

  if (n_actual != n_expected) {
    PyErr_SetString(PyExc_ValueError,
                    "Actual and expected must have the same size.");
  }

  size_t tp, fp, fn, tn;
  confusion_matrix(actual, expected, &tp, &fp, &fn, &tn);

  score = validation_func(tp, fp, fn, tn);
  return Py_BuildValue("d", score);
}

/* Evaluation measures */
static double jaccard_index(size_t tp, size_t fp, size_t fn,
                            size_t Py_UNUSED(tn))
{
  return (double)tp / (fp + fn);
}

static double accuracy(size_t tp, size_t fp, size_t fn, size_t tn)
{
  return (double)(tp + tn) / (tp + fp + fn + tn);
}

static double f1_score(size_t tp, size_t fp, size_t fn, size_t Py_UNUSED(tn))
{
  double precision = (double)tp / (tp + fp);
  double recall = (double)tp / (tp + fn);

  return (2 * precision * recall) / (precision + recall);
}

static double error_rate(size_t tp, size_t fp, size_t fn, size_t tn)
{
  return (double)(fp + fn) / (tp + fp + fn + tn);
}

/* Python Wrappers */
static PyObject *val_jaccard(PyObject *Py_UNUSED(self), PyObject *args)
{
  return validation_template(args, jaccard_index);
}

static PyObject *val_accuracy(PyObject *Py_UNUSED(self), PyObject *args)
{
  return validation_template(args, accuracy);
}

static PyObject *val_f1(PyObject *Py_UNUSED(self), PyObject *args)
{
  return validation_template(args, f1_score);
}

static PyObject *val_error_rate(PyObject *Py_UNUSED(self), PyObject *args)
{
  return validation_template(args, error_rate);
}

static PyObject *val_confusion_matrix(PyObject *Py_UNUSED(self),
                                      PyObject *args)
{
  PyArrayObject *actual, *expected;
  PyObject *res;

  if (!PyArg_ParseTuple(args, "O!O!",
                        &PyArray_Type, &actual,
                        &PyArray_Type, &expected)) {
    return NULL;
  }

  npy_intp n_actual = PyArray_DIM(actual, 0);
  npy_intp n_expected = PyArray_DIM(expected, 0);

  if (n_actual != n_expected) {
    PyErr_SetString(PyExc_ValueError,
                    "Actual and expected must have the same size.");
  }

  size_t conf_mat[4];
  confusion_matrix(actual, expected, conf_mat, conf_mat + 1, conf_mat + 2,
                   conf_mat + 3);

  res = PyTuple_New(4);
  for (size_t i = 0; i < 4; i++) {
    PyTuple_SetItem(res, i, PyLong_FromLong(conf_mat[i]));
  }
  return res;
}

static PyMethodDef ValidationMethods[] = {
  {"jaccard_index", val_jaccard, METH_VARARGS, NULL},
  {"accuracy", val_accuracy, METH_VARARGS, NULL},
  {"f1_score", val_f1, METH_VARARGS, NULL},
  {"error_rate", val_error_rate, METH_VARARGS, NULL},
  {"confusion_matrix", val_confusion_matrix, METH_VARARGS, NULL},
  {NULL, NULL, 0, NULL}
};

static struct PyModuleDef validate_module = {
  .m_base = PyModuleDef_HEAD_INIT,
  .m_name = "_validate",
  .m_size = -1,
  .m_methods = ValidationMethods
};

PyMODINIT_FUNC PyInit__validate(void)
{
  import_array();

  return PyModule_Create(&validate_module);
}
