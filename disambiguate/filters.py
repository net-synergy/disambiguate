__all__ = ["before", "between", "since"]

import numpy as np
from pubnet import PubNet


def before(year: int, net: PubNet) -> PubNet:
    return net.where(
        "Publication", lambda date: date.feature_vector("Year") < year
    )


def since(year: int, net: PubNet) -> PubNet:
    return net.where(
        "Publication", lambda date: date.feature_vector("Year") > year
    )


def between(start: int, end: int, net: PubNet) -> PubNet:
    return net.where(
        "Publication",
        lambda date: np.logical_and(
            date.feature_vector("Year") <= end,
            date.feature_vector("Year") >= start,
        ),
    )
