"""Methods to validate results of disambiguation algorithms.

The provided methods use ORCIDS as ground truth to evaluate the quality of the
labels.
"""

__all__ = [
    "jaccard_index",
    "accuracy",
    "f1_score",
    "error_rate",
    "confusion_matrix",
]

from typing import Callable

import numpy as np
from pubnet import PubNet

import disambiguate._validate as _validate


def _validator_factory(val_func) -> Callable[[PubNet, np.ndarray], float]:
    def validator(net: PubNet, labels: np.ndarray) -> float:
        orcids = net.get_edge("Author", "Orcid")
        labels = labels[orcids["Author"]]
        return val_func(labels, orcids["Orcid"])

    return validator


jaccard_index = _validator_factory(_validate.jaccard_index)
accuracy = _validator_factory(_validate.accuracy)
f1_score = _validator_factory(_validate.f1_score)
error_rate = _validator_factory(_validate.error_rate)


def confusion_matrix(
    net: PubNet, labels: np.ndarray
) -> tuple[int, int, int, int]:
    """Calculate the confusion matrix between orcids and labels.

    Orcids are assumed to be the ground truth and labels are the predicted
    labels.

    Returns
    -------
    tp : int
      True positive, number of pairs where orcids and labels both match.
    fp : int
      False positive, number of pairs where labels match but orcids do not.
    fn : int
      False negative, number of pairs where labels don't match but orcids do.
    tn : int
      True negative, number of pairs where labels and orcids both do not match.
    """
    orcids = net.get_edge("Author", "Orcid")
    labels = labels[orcids["Author"]]
    return _validate.confusion_matrix(labels, orcids["Orcid"])
