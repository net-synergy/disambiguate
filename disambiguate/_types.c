#define PY_SSIZE_T_CLEAN
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <stddef.h>
#include <omp.h>

#include "types.h"

static PyObject *EdgeList_new(PyTypeObject *type, PyObject *args,
                              PyObject *kwds)
{
  EdgeList *self = (EdgeList *)type->tp_alloc(type, 0);
  self->n_rows = 0;

  return (PyObject *)self;
}

static int _is_sorted(PyArrayObject *arr)
{
  for (npy_int i = 1; i < PyArray_DIM(arr, 0); i++) {
    if (*(size_t *)PyArray_GETPTR1(arr, i) <
        * (size_t *)PyArray_GETPTR1(arr, i - 1)) {
      return 0;
    }
  }

  return 1;
};

static int EdgeList_init(EdgeList *self, PyObject *args, PyObject *kwds)
{
  static char *kwlist[] = {"from_nodes", "to_nodes", "tfidf", NULL};
  PyArrayObject *row_list = NULL, *col_list = NULL;
  int tfidf = 0;

  if (!PyArg_ParseTupleAndKeywords(args, kwds, "|O!O!p", kwlist,
                                   &PyArray_Type, &row_list,
                                   &PyArray_Type, &col_list,
                                   &tfidf)) {
    return -1;
  }

  if (!_is_sorted(row_list)) {
    PyErr_SetString(PyExc_ValueError,
                    "Rows must be sorted");
    return -1;
  }

  if (!(PyArray_DIM(row_list, 0) == PyArray_DIM(col_list, 0))) {
    PyErr_SetString(PyExc_ValueError, "Node lists must be the same size");
    return -1;
  }

  self->n_rows = *(size_t *)PyArray_GETPTR1(row_list,
                 PyArray_DIM(row_list, 0) - 1) + 1;
  self->rows = PyMem_MALLOC(sizeof(*self->rows) * (self->n_rows + 1));
  self->n_unique_rows = 0;
  EDGE_NNZ(self) = (size_t)PyArray_DIM(row_list, 0);
  self->cols = PyMem_MALLOC(sizeof(*self->cols) * EDGE_NNZ(self));

  for (size_t i = 0; i < EDGE_NNZ(self); i++) {
    self->cols[i] = *(size_t *)PyArray_GETPTR1(col_list, i);
  }

  self->n_cols = 0;
  for (size_t i = 0; i < EDGE_NNZ(self); i++) {
    self->n_cols = self->n_cols > EDGE_COL(self, i) ?
                   self->n_cols : EDGE_COL(self, i);
  }
  self->n_cols++;

  self->rows[0] = 0;
  self->n_unique_rows += *(size_t *)PyArray_GETPTR1(row_list, 0) == 0;
  size_t current = 0, prev = 0;
  for (size_t i = 0; i < EDGE_NNZ(self); i++) {
    current = *(size_t *)PyArray_GETPTR1(row_list, i);

    if (prev != current) {
      self->n_unique_rows++;
    }

    while (prev != current) {
      self->rows[++prev] = i;
    }
  }

  EL_sort_rows(self);

  if (tfidf) {
    EL_tfidf(self);
  }

  return 0;
}

static void EdgeList_dealloc(EdgeList *self)
{
  Py_XDECREF(self->rows);
  Py_XDECREF(self->cols);

  if (self->authors) {
    Py_XDECREF(self->authors);
  }

  if (self->weights) {
    Py_XDECREF(self->weights);
  }

  Py_TYPE(self)->tp_free((PyObject *) self);
}

static PyObject *EdgeList_repr(EdgeList *self)
{
  if (self->n_rows == 0) {
    return PyUnicode_FromFormat("EdgeList{size:%d}", 0);
  } else if (self->n_rows < 8) {
    // TODO: Display rows.
    return PyUnicode_FromFormat("EdgeList{size:%d}", self->n_rows);
  } else {
    return PyUnicode_FromFormat("EdgeList{size:%d}", self->n_rows);
  }
}

static PyObject *EdgeList_str(EdgeList *self)
{
  return PyUnicode_FromFormat("EdgeList{n_rows:%d}", self->n_rows);
}

static PyTypeObject EdgeType = {
  .ob_base = PyVarObject_HEAD_INIT(NULL, 0)
  .tp_name = "_types.EdgeList",
  .tp_doc = PyDoc_STR("Sparse representation for edges"),
  .tp_basicsize = sizeof(EdgeList),
  .tp_itemsize = 0,
  .tp_flags = Py_TPFLAGS_DEFAULT,
  .tp_new = EdgeList_new,
  .tp_init = (initproc)EdgeList_init,
  .tp_dealloc = (destructor)EdgeList_dealloc,
  .tp_repr = (reprfunc)EdgeList_repr,
  .tp_str = (reprfunc)EdgeList_str,
};

static struct PyModuleDef types_module = {
  .m_base = PyModuleDef_HEAD_INIT,
  .m_name = "_types",
  .m_size = -1,
};

PyMODINIT_FUNC PyInit__types(void)
{
  import_array();

  PyObject *module = PyModule_Create(&types_module);

  if (PyType_Ready(&EdgeType) < 0) {
    return NULL;
  }

  Py_INCREF(&EdgeType);
  PyModule_AddObject(module, "EdgeList", (PyObject *) &EdgeType);

  return module;
}
