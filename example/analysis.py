import numpy as np
from pubnet import PubNet

from disambiguate.similarity import merge_within, to_edgelist
from disambiguate.validate import accuracy, error_rate, f1_score, jaccard_index

## Start by using last name for labels
publications = PubNet.load_graph("test_pubmed")
author_net = PubNet.load_graph(
    "test_pubmed", root="Author", edges=(("Author", "*"),)
)
valid_ids = np.unique(author_net.get_edge("Author", "LastName")["Author"])
author_net = author_net[valid_ids]
publications.update(author_net)
valid_ids = np.unique(
    publications.get_edge("Author", "Publication")["Publication"]
)
publications = publications[valid_ids]
labels = author_net.get_edge("Author", "LastName")["LastName"].copy()

## Convert to EdgeLists
pub_edges = to_edgelist(publications, ("Author", "Reference"))
pub_weights = {k: 0.0 for k in pub_edges}
author_edges = to_edgelist(author_net, ("LastName", "FirstInitial"))
author_weights = {k: 0.0 for k in author_edges}

## Quality of Last name based labels
print(jaccard_index(publications, labels))
print(accuracy(publications, labels))
print(f1_score(publications, labels))
print(error_rate(publications, labels))

## Break up Last name grouping into First initial + Last name groupings.
author_weights["FirstInitial"] = 1.0
merge_within(
    labels,
    pub_edges,
    author_edges,
    threshold=0.85,  # Really a binary comparison; First initials match or not.
    pub_weights=pub_weights,
    author_weights=author_weights,
)
print(jaccard_index(publications, labels))
print(accuracy(publications, labels))
print(f1_score(publications, labels))
print(error_rate(publications, labels))

## Perform publication based refining
author_weights["FirstInitial"] = 0.0
pub_weights = {"Author": 0.8, "Reference": 0.2}
merge_within(
    labels,
    pub_edges,
    author_edges,
    threshold=0.2,
    pub_weights=pub_weights,
    author_weights=author_weights,
)
print(jaccard_index(publications, labels))
print(accuracy(publications, labels))
print(f1_score(publications, labels))
print(error_rate(publications, labels))
