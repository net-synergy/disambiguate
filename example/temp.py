import numpy as np
from pubnet import PubNet

from disambiguate import filters
from disambiguate.similarity import merge_between, merge_within
from disambiguate.validate import jaccard_index

## Start by using first and last names for labels
publications = PubNet.load_graph("test_pubmed")
author_net = PubNet.load_graph(
    "test_pubmed", root="Author", edges=(("Author", "*"),)
)
valid_ids = np.unique(author_net.get_edge("Author", "LastName")["Author"])
author_net = author_net[valid_ids]
publications.update(author_net)
valid_ids = np.unique(
    publications.get_edge("Author", "Publication")["Publication"]
)
publications = publications[valid_ids]

# Temp: shrink network
n_authors = 100000
author_ids = np.arange(n_authors)
pub_ids = publications.get_edge("Author", "Publication")[
    publications.get_edge("Author", "Publication")["Author"] < n_authors,
    "Publication",
]
publications = publications[pub_ids]
author_net = author_net[author_ids]
publications.update(author_net)

# labels = np.zeros(author_ids.shape, dtype=np.int64)
labels = author_net.get_edge("Author", "LastName")["LastName"]
old_labels = labels.copy()

print(jaccard_index(publications, labels))
labels = merge_within(
    publications, labels, threshold=0.225, coauthor_weight=0.5
)
print(jaccard_index(publications, labels))

# thresholds = np.arange(0.0, 0.5, 0.1)
# weights = np.arange(0.0, 1.1, 0.1)
# t = 0.2
# w = 0.5
# labels = old_labels.copy()
# for i in range(10):
#     merge_within(publications, labels, threshold=t, coauthor_weight=w)
#     print(i, t, w, jaccard_index(publications, labels))

# publications = filters.between(2020, 2022, publications)
