import os

import numpy as np
from pubnet import PubNet

from disambiguate.similarity import merge_within, score_features, to_edgelist
from disambiguate.validate import confusion_matrix

## Set up environment
CACHE_LABELS = True
RECACHE_LABELS = False
LABEL_PATH = "cache/labels_train.npy"
labels_exist = os.path.exists(LABEL_PATH)

if RECACHE_LABELS and labels_exist:
    labels_exist = False
    os.unlink(LABEL_PATH)

## Start by using last name for labels
publications = PubNet.load_graph("test_pubmed")
author_net = PubNet.load_graph(
    "test_pubmed", root="Author", edges=(("Author", "*"),)
)
valid_ids = np.unique(author_net.get_edge("Author", "LastName")["Author"])
author_net = author_net[valid_ids]
publications.update(author_net)
valid_ids = np.unique(
    publications.get_edge("Author", "Publication")["Publication"]
)
publications = publications[valid_ids]

if labels_exist:
    labels = np.load(LABEL_PATH)
else:
    labels = author_net.get_edge("Author", "LastName")["LastName"].copy()
    tp, fp, fn, tn = confusion_matrix(publications, labels)
    print(f"Percent orcids with same last name {tp} / {tp + fn}")

## Convert to EdgeLists (TODO: Try with and without TFIDF)
pub_edges = to_edgelist(
    publications,
    ("Author", "Reference", "Chemical", "Descriptor", "Qualifier", "Keyword"),
)
author_edges = to_edgelist(author_net, ("FirstInitial", "Orcid"))

## Break up Last name grouping into First initial + Last name groupings.
if not labels_exist:
    pub_weights = {k: 0.0 for k in pub_edges}
    author_weights = {k: 0.0 for k in author_edges}
    author_weights["FirstInitial"] = 1.0
    merge_within(
        labels,
        pub_edges,
        author_edges,
        threshold=0.85,  # Really a binary comparison; First initials match?
        pub_weights=pub_weights,
        author_weights=author_weights,
    )
    np.save(LABEL_PATH, labels)

tp, fp, fn, tn = confusion_matrix(publications, labels)
print(f"Percent orcids with same last name + first initial {tp} / {tp + fn}")

## Score features
scores = score_features(
    labels,
    author_edges["Orcid"],
    pub_edges,
    min_occurances=2,
)
print(scores)
