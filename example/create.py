from pubnet import PubNet
from pubnet.download import from_pubmed

from disambiguate import clean

## Collect graph
file_numbers = range(1200, 1205)
node_list = [
    {"name": "publication", "value": "date"},
    {
        "name": "author",
        "value": ["last_name", "fore_name", "orcid", "affiliation"],
        "grouping": "relational",
    },
    "chemical",
    "qualifier",
    "descriptor",
    "keyword",
    "reference",
]

# Reminder, need to update pubnet before running.
net = from_pubmed(file_numbers, node_list, "test_pubmed", load_graph=True)
net.drop(nodes="Reference")
# net.save_graph(file_format="binary", overwrite=True)

# ## Load and modify author network
# net = PubNet.load_graph("test_pubmed", root="Publication")

clean.email(net)
clean.orcid(net)
clean.first_initial(net)

net.save_graph(file_format="binary", overwrite=True)
